
package classes;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.imageio.ImageIO;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EditorViewController implements Initializable {
    //TOP MENU
    @FXML
    public MenuItem newMenuItem;
    @FXML
    public MenuItem openMenuItem;
    @FXML
    public MenuItem saveMenuItem;
    @FXML
    public MenuItem closeMenuItem;
    @FXML
    public MenuItem undoMenuItem;
    @FXML
    public MenuItem redoMenuItem;
    //LEFT TOOLBAR
    @FXML
    public ToolBar toolBar;

    //LEFT TOOLBAT BUTTONS
    @FXML
    public ToggleButton brushButton;
    @FXML
    public ToggleButton handButton;
    @FXML
    public ToggleButton lineButton;
    @FXML
    public ToggleButton ellipseButton;
    @FXML
    public ToggleButton triangleButton;
    @FXML
    public ToggleButton rectangleButton;

    @FXML
    public ColorPicker colorPicker;
    @FXML
    public Slider sizePicker;
    @FXML
    public CheckBox fillCheckBox;
    // CENTER
    @FXML
    public ScrollPane scrollPane;
    @FXML
    public Label labelSizePicker;
    @FXML
    private Canvas canvas;
    private GraphicsContext gcec;
    @FXML
    private Canvas effectCanvas;
    private GraphicsContext gc;


    double startX, startY, lastX, lastY, oldX, oldY;
    double hg;
    private String currentCommand;
    private ToggleButton currentToolButton;

    private String fileStr = "";
    private Stage primaryStage;

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //HOT KEYS
        newMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        openMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        saveMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        closeMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.ALT_DOWN));


        gc = canvas.getGraphicsContext2D();
        gcec = effectCanvas.getGraphicsContext2D();
        colorPicker.setValue(Color.BLACK);
        clearCanvas();
        sizePicker.setMin(1);
        sizePicker.setMax(50);
        sizePicker.setShowTickMarks(true);
        sizePicker.setShowTickLabels(true);
        sizePicker.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                labelSizePicker.textProperty().setValue(String.format("%.1f", newValue.doubleValue()));
                gc.setLineWidth((Double) newValue);
                gcec.setLineWidth((Double) newValue);
            }
        });

    }


    private void clearCanvas() {
        gc.setFill(Color.WHITE);
        gcec.setFill(Color.WHITE);
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gcec.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    @FXML
    public void drawButtonPressed(ActionEvent event) {

        if (((ToggleButton) (event.getSource())).isSelected()) {
            if (currentCommand != null) {
                currentToolButton.setSelected(false);
            }

            currentToolButton = ((ToggleButton) (event.getSource()));
            currentCommand = currentToolButton.getId();


        } else if (Objects.equals(currentCommand, ((ToggleButton) (event.getSource())).getId())) {
            currentCommand = null;
        }
    }

    // COMPLETE CANVAS

    @FXML
    private void onMousePressedListener(MouseEvent e) {
        this.startX = e.getX();
        this.startY = e.getY();
        this.oldX = e.getX();
        this.oldY = e.getY();
    }

    @FXML
    private void onMouseDraggedListener(MouseEvent e) {
        this.lastX = e.getX();
        this.lastY = e.getY();

        if (Objects.equals(currentCommand, rectangleButton.getId())) {
            drawRectEffect();
        }
        if (Objects.equals(currentCommand, ellipseButton.getId()))
            drawOvalEffect();
        if (Objects.equals(currentCommand, lineButton.getId()))
            drawLineEffect();
        if (Objects.equals(currentCommand, brushButton.getId()))
            freeDrawing();
        if (Objects.equals(currentCommand, handButton.getId())) {
            handTranslate();
        }
        if (Objects.equals(currentCommand, triangleButton.getId())) {
            triangleDrawingEffect();
        }

    }

    @FXML
    private void onMouseReleaseListener(MouseEvent e) {

        if (Objects.equals(currentCommand, rectangleButton.getId()))
            drawRect();
        if (Objects.equals(currentCommand, ellipseButton.getId()))
            drawOval();
        if (Objects.equals(currentCommand, lineButton.getId()))
            drawLine();

        if (Objects.equals(currentCommand, triangleButton.getId())) {
            triangleDrawing();
        }
    }

    ////////////////////////////////////
    private void handTranslate() {
        canvas.setTranslateX(canvas.getTranslateX() + lastX - startX);
        canvas.setTranslateY(canvas.getTranslateY() + lastY - startY);

        effectCanvas.setTranslateX(canvas.getTranslateX() + lastX - startX);
        effectCanvas.setTranslateY(canvas.getTranslateY() + lastY - startY);
    }

    private void drawOval() {

        double wh = Math.abs(lastX - startX);
        double hg = Math.abs(lastY - startY);
        if (fillCheckBox.isSelected()) {
            gc.setFill(colorPicker.getValue());
            gc.fillOval(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        } else {
            gc.setStroke(colorPicker.getValue());
            gc.strokeOval(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        }
    }

    private void drawRect() {
        double wh = Math.abs(lastX - startX);
        double hg = Math.abs(lastY - startY);
        if (fillCheckBox.isSelected()) {
            gc.setFill(colorPicker.getValue());
            gc.fillRect(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        } else {
            gc.setStroke(colorPicker.getValue());
            gc.strokeRect(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        }

    }

    private void drawLine() {

        gc.setStroke(colorPicker.getValue());
        gc.strokeLine(startX, startY, lastX, lastY);
    }

    private void freeDrawing() {
        gc.setStroke(colorPicker.getValue());
        gc.strokeLine(oldX, oldY, lastX, lastY);
        oldX = lastX;
        oldY = lastY;
    }

    private void triangleDrawing() {

        double wh = lastX - startX;
        if (fillCheckBox.isSelected()) {
            gc.setFill(colorPicker.getValue());
            gc.fillPolygon(new double[]{lastX, wh, startX},
                    new double[]{lastY, startY, lastY}, 3);
        } else {
            gc.setStroke(colorPicker.getValue());
            gc.strokePolygon(new double[]{lastX, wh, startX},
                    new double[]{lastY, startY, lastY}, 3);
        }
    }

    //////////////////////////////////////////
    private void drawOvalEffect() {
        double wh = Math.abs(lastX - startX);
        double hg = Math.abs(lastY - startY);
        gcec.clearRect(0, 0, effectCanvas.getWidth(), effectCanvas.getHeight());
        if (fillCheckBox.isSelected()) {
            gcec.setFill(colorPicker.getValue());
            gcec.fillOval(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        } else {
            gcec.setStroke(colorPicker.getValue());
            gcec.strokeOval(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        }
    }

    private void drawRectEffect() {

        double wh = Math.abs(lastX - startX);
        double hg = Math.abs(lastY - startY);
        gcec.clearRect(0, 0, effectCanvas.getWidth(), effectCanvas.getHeight());
        if (fillCheckBox.isSelected()) {
            gcec.setFill(colorPicker.getValue());
            gcec.fillRect(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        } else {
            gcec.setStroke(colorPicker.getValue());
            gcec.strokeRect(Math.min(startX, lastX), Math.min(startY, lastY), wh, hg);
        }
    }

    private void drawLineEffect() {

        gcec.setStroke(colorPicker.getValue());
        gcec.clearRect(0, 0, effectCanvas.getWidth(), effectCanvas.getHeight());
        gcec.strokeLine(startX, startY, lastX, lastY);


    }

    private void triangleDrawingEffect() {

        double wh = lastX - startX;
        //  double hg = Math.abs(lastY - startY);

        gcec.clearRect(0, 0, effectCanvas.getWidth(), effectCanvas.getHeight());
        if (fillCheckBox.isSelected()) {
            gcec.setFill(colorPicker.getValue());
            gcec.fillPolygon(new double[]{lastX, wh, startX},
                    new double[]{lastY, startY, lastY}, 3);
        } else {
            gcec.setStroke(colorPicker.getValue());
            gcec.strokePolygon(new double[]{lastX, wh, startX},
                    new double[]{lastY, startY, lastY}, 3);
        }
    }

    ////////////////////////////////////////
    @FXML
    private void onMouseExitedListener(MouseEvent event) {
        //System.out.println("No p    uedes dibujar fuera del canvas");
    }

    //COMPLETE MENU

    @FXML
    public void newMenuItemPressed(ActionEvent event) {
        clearCanvas();

    }

    @FXML
    public void openMenuItemPressed(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image", "*.png", "*.jpg", "*,jpeg", "*.bmp"));
        File f = fc.showOpenDialog(primaryStage);
        if (f != null) {
            try {
                fileStr = f.getAbsolutePath();
                Image i = new Image(new FileInputStream(f));
                clearCanvas();
                gc.drawImage(i, 0, 0);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(EditorViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    public void saveMenuItemPressed(ActionEvent event) {
        File f;
        String extension = "png";
        if (fileStr.length() > 0) {
            //neu la file dc mo thi chi luu lai
            f = new File(fileStr);
        } else {
            //neu la file tao moi thi hien ban luu
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PNG Image", "*.png"),
                    new FileChooser.ExtensionFilter("JPG Image", "*.jpg"),
                    new FileChooser.ExtensionFilter("JPEG Image", "*,jpeg"),
                    new FileChooser.ExtensionFilter("Bitmap Image", "*.bmp"));
            f = fc.showSaveDialog(primaryStage);
        }

        if (f == null) {
            return;
        }

        int i = f.getName().lastIndexOf('.');
        int p = Math.max(f.getName().lastIndexOf('/'), f.getName().lastIndexOf('\\'));

        if (i > p) {
            extension = f.getName().substring(i + 1);
        }
//ADD IMAGE ON CANVAS
        try {
            WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
            canvas.snapshot(null, writableImage);
            RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
            ImageIO.write(renderedImage, extension, f);
        } catch (IOException ex) {
            Logger.getLogger(EditorViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void closeMenuItemPressed(ActionEvent event) {


        Platform.exit();
    }

    @FXML
    public void undoMenuItemPressed(ActionEvent event) {

    }

    @FXML
    public void redoMenuItemPressed(ActionEvent event) {

    }
}

