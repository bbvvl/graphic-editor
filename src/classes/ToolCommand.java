
package classes;


public abstract class ToolCommand {
    
    public abstract void enable();
    public abstract void disable();
    
}