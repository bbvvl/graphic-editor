import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import classes.EditorViewController;


public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/editorDesign.fxml"));
        Parent root = loader.load();
        EditorViewController view = loader.getController();
        view.setPrimaryStage(stage);
        Scene scene = new Scene(root);

        stage.setMinHeight(350);
        stage.setMinWidth(300);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
